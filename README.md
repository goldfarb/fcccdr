# Franklin County Community COVID Data Repository

This repository structure is an example for a project, and is not in any way part of the work that Franklin County or the Department of Elementary and Secondary Education is doing with COVID data collection.

The repository includes:

* [Welcome](docs/index.md)
* [Collection Policy](docs/collection_policy.md)
* [Data Preparation](docs/data_preparation.md)
* [Metadata Elements](docs/metadata_elements.md)
* [Access](docs/access.md)
* [License & Attribution](docs/license.md)
* [Versioning](docs/versioning.md)
* [Engagement & Collaboration](docs/engagement.md)
* [References](docs/references.md)
