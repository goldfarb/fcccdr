## Welcome to the Franklin County
## Community COVID Data Repository

We understand that the data collected here, like all other data, are incomplete.

This repository of COVID-19 data from Franklin County is a work in progress. It is assembled by and for the county’s school communities, and is overseen by the Community Data Curator [data_curator@...]().

#### Needs

School administrators need the most up-to-date information about the COVID-19 situation in their school communities, in order to make pragmatic decisions that affect the health and safety of the students, teachers, and staff of their schools, as well as impacting the larger community in which the school operates.

#### Practices

The data collected in this repository will be available for all community members to access and analyze. The Data Curator will strive to make the materials available in a timely way, with the understanding that some data cleaning and standardizing will be necessary.

The work of this repository is a [collaborative endeavor](../engagement) of the community, and is a work in progress.
