## Metadata Elements
***

### Metadata for Communications

| Field | Name | Values, Format | Type | Required | Notes |
| :--- | :--- | :--- | :--- | :--- | :--- |
| date | Submission Date | YYYYMMDD | string | required |  |
| district | School District |  | string | required |  |
| school | School Name |  | string | recommended | Not necessary for district-wide communications. |
| submitter | Submitter | self<br> school<br> other | string | required |  |
| source | Souce | name<br> role<br> department | string | required | Whose name is on the communication, as well as who contributed to the decision. |
| method | Method | email<br> note home<br> press conference<br> other | string | required | If multiple methods were used, select other and indicate all methods. |
| com | Communication | text | string | required |  |

### Metadata for Policy Documents

| Field | Name | Values, Format | Type | Required | Notes |
| :--- | :--- | :--- | :--- | :--- | :--- |
| date | Submission Date | YYYYMMDD | string | required |  |
| district | School District |  | string | required |  |
| school | School Name |  | string | recommended | Not necessary for district-wide policy. |
| submitter | Submitter | self<br> school<br> other | string | required |  |
| source | Souce | name<br> role<br> department | string | required | Whose name is on the policy document, as well as who contributed to the decision. |
| end_date | End Date | YYYYMMDD | string | recommended | If the policy has a fixed time period, the date on which it ends. |
| policy | Policy | text | string | required |  |

### Metadata for School-Wide Data

_If possible, submit school-wide data with multiple fields concurrently. That is, a submission with the student population and the number of students currently out is more useful than submitting those two datapoints separately._

| Field | Name | Values, Format | Type | Required | Notes |
| :--- | :--- | :--- | :--- | :--- | :--- |
| date | Submission Date | YYYYMMDD | string | required |  |
| district | School District |  | string | required |  |
| school | School Name |  | string | required |  |
| submitter | Submitter | self, school, other | string | required |  |
| student_pop | Student Population | 0 | integer | recommended | The number of enrolled students. |
| staff_pop | Staff Population | 0 | integer | recommended | The number of faculty and staff, including bus drivers, part-time employees, and any other personnel whose tests will be included in reported data. |
| student_out | Students Out | 0 | integer | recommended | The number of students currently out because of a positive test, displaying COVID symptoms, or close exposure. |
| staff_out | Staff Out | 0 | integer | recommended | The number of faculty and staff currently out because of a positive test, displaying COVID symptoms, or close exposure. This may include staff who are out to care for a family member. |

### Metadata for Test and Case Data

| Field | Name | Values, Format | Type | Required | Notes |
| :--- | :--- | :--- | :--- | :--- | :--- |
| first_test_date | First Test Date | YYYYMMDD | string | recommended |  |
| final_test_date | Final Test Date | YYYYMMDD | string | recommended |  |
| date | Submission Date | YYYYMMDD | string | required |  |
| test_type | Test Type | PCR <br> antigen <br> pool <br> other | string | recommended |  |
| test_make | Test Manufacturer | BinaxNOW <br> Flowflex <br> iHealth <br> other | string | optional |  |
| case | Case | T <br> F | boolean | required |  |
| role | Community Role | student <br> teacher <br> staff <br> parent <br> sibling <br> other | string | recommended |  |
| admin | Test Administered | home <br> school | string | recommended |  |
| district | School District |  | string | required |  |
| school | School Name |  | string | required |  |
| submitter | Submitter | self <br> school <br> other | string | required |  |
| age | Age | 0 | integer | required |  |
| gender | Gender | female <br> male <br> non-binary <br> other |  | optional |  |
| zip | ZIP code | 00000 | string | recommended | ZIP code for home address; for students whose adults live in different ZIP codes, choose one |
| ethnicity | ethnicity | Latinx <br> Not Latinx <br> Other | string | recommended | Using the Census categories for comparisons |
| race | Race | American Indian or Alaska Native <br> Asian <br> Black or African American<br> Native Hawaiian or Other Pacific Islander <br> White <br> Other | string | recommended | Using the Census categories for comparisons |

> ***
For questions about the metadata elements, please contact the Community Data Curator ([data_curator@...]()). Given time and workload limitations, questions from individuals may not be answered in a timely manner, as supporting the schools and districts is the focus of the   Franklin County Community COVID Data Repository.
