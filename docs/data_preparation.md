## Data Preparation
***
### Data Preparation for Communications

Communications materials may be submitted as structured data, as a CSV named as ` CODE_com_YYYYMMDD.csv`, where CODE is the school's four character identification code. To keep the repository current, no more than a week of communications should be combined before submission.

Alternatively, communications materials may be submitted through the portal. The portal will accept the communication itself as TXT or PDF. The metadata needed is detailed [here](../metadata_elements/#metadata-for-communications).

### Data Preparation for Policy Documents

Schools and districts may decide which submitted materials should be designated as communications and which as policy documents. Communications alerting the community to updated policies should be included, in addition to the policies themselves.

Policy documents may be submitted as structured data, as a CSV named as ` CODE_pol_YYYYMMDD.csv`, where CODE is the school's four character identification code. To keep the repository current, no more than a week of policy documents should be combined before submission.

Alternatively, policy documents may be submitted through the portal. The portal will accept the document itself as TXT or PDF. The metadata needed is detailed [here](../metadata_elements/#metadata-for-policy-documents).


### Data Preparation for School-Wide Data

School-wide data may be submitted as structured data, as a CSV named as ` CODE_swd_YYYYMMDD.csv`, where CODE is the school's four character identification code. To keep the repository current, no more than a week of school-wide data should be combined before submission. More frequent submissions are preferred.

Alternatively, school-wide data may be submitted through the portal. The metadata needed is detailed [here](../metadata_elements/#metadata-for-school-wide-data).


### Data Preparation for Test and Case Data

Test and case data may be submitted as structured data, as a CSV named as ` CODE_tcd_YYYYMMDD.csv`, where CODE is the school's four character identification code. To keep the repository current, no more than a week of test and case data should be combined before submission. More frequent submissions are preferred.

Alternatively, test and case data may be submitted through the portal. The metadata needed is detailed [here](../metadata_elements/#metadata-for-test-and-case-data). Individual community members will need to use the portal to report. For parents with students in more than one school, please report a test only once.



> ***
For questions about data preparation, please contact the Community Data Curator ([data_curator@...]()). Given time and workload limitations, questions from individuals may not be answered in a timely manner, as supporting the schools and districts is the focus of the   Franklin County Community COVID Data Repository.
