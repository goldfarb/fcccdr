## License and Attribution
***

The data, metadata, and other content is shared under a [Creative Commons CC BY 4.0](https://creativecommons.org/licenses/by/4.0/) license, which requires users to attribute the source and license type (CC BY 4.0) when sharing work built on these materials.

Attribution should be made to the Franklin County Community COVID Data Repository, or to Community COVID Data.


> ***
For questions about licensing and attribution, please contact the Community Data Curator ([data_curator@...]()). Given time and workload limitations, questions from individuals may not be answered in a timely manner, as supporting the schools and districts is the focus of the   Franklin County Community COVID Data Repository.
