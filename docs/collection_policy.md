## Collection Policy
***
### Submission Guidelines

All materials must be submitted by a member of the school community.

* Each school or district may determine who is considered part of their community, and the information about this decision should be included in the policy documents in the repository.
* Individuals will have the chance to indicate if they may be considered part of additional communities, and if their test results may be included in more than one dataset.
* Some types of data ([communications](../data_preparation/#data-preparation-for-communications), [policy documents](../data_preparation/##data-preparation-for-policy-documents), and [school-wide data](../data_preparation/#data-preparation-for-communications)) must be submitted by a representative of the school. [Test and case data](../data_preparation/#data-preparation-for-test-and-case-data) may be submitted by a representative of the school or a community member. Community members are able to submit their test information via the repository’s portal.


### Structure
The data collected will be of three main types, structured data about COVID-19 tests and cases, structured data about the school-wide situation, and policy documents and other communication from the school to provide additional context for any set of tests. For example, when looking at the number of tests recorded by a district for a particular week, the accompanying communications materials will help contextualize who was being asked to test and how often at the time. Additional school-level data will also be collected, including the total number of enrolled students, the total number of faculty and staff, and data about the number of students and employees currently absent because of COVID-19.

The guidelines for [preparing the data](../data_preparation/) and [metadata](../metadata_elements/) are intended to provide structure and continuity, so that school personnel and the Data Curator are able to track changes over time and make comparisons to the overall community. Any field with a set of suggested values also allows for freeform answers. We decided that while this may make future data analysis more complex, it will also allow people to submit information that is more accurate. The set of suggested values may be updated to reflect additional options from community data.


> ***
For questions about the collection policy, please contact the Community Data Curator ([data_curator@...]()). Given time and workload limitations, questions from individuals may not be answered in a timely manner, as supporting the schools and districts is the focus of the   Franklin County Community COVID Data Repository.
