## References
***
#### Supplemental Data Sources

| Creator | Title | URL |
| :--- | :--- | :--- |
| Cornell University Library | Cornell's Digital Repository: Policies | [guides.library.cornell.edu/ecommons/policy](https://guides.library.cornell.edu/ecommons/policy) |
| DataSF | Submission Guidelines | [datasf.org/publishing/submission-guidelines/](https://datasf.org/publishing/submission-guidelines/) |
| University of Michigan ICPSR | Share Data | [icpsr.umich.edu/web/pages/deposit/index.html](https://www.icpsr.umich.edu/web/pages/deposit/index.html) |
| University of Michigan Library | Deep Blue Data | [deepblue.lib.umich.edu/data/about?locale=en](https://deepblue.lib.umich.edu/data/about?locale=en) |
| University of Minnesota Libraries Digital Conservancy | Data Repository for U of M (DRUM) | [conservancy.umn.edu/pages/drum/policies/#data-collection-policy](https://conservancy.umn.edu/pages/drum/policies/#data-collection-policy) |


#### Data Repositories Referenced

| Creator | Title | URL |
| :--- | :--- | :--- |
| CDC | COVID-19 Case Surveillance Public Use Data with Geography | [data.cdc.gov/Case-Surveillance/COVID-19-Case-Surveillance-Public-Use-Data-with-Ge/n8mc-b4w4](https://data.cdc.gov/Case-Surveillance/COVID-19-Case-Surveillance-Public-Use-Data-with-Ge/n8mc-b4w4) |
| Massachusetts Department of Public Health | Reports from the Massachusetts Department of Public Health (DPH) | [mass.gov/info-details/archive-of-covid-19-cases-in-massachusetts](https://www.mass.gov/info-details/archive-of-covid-19-cases-in-massachusetts) |
| Public Health Institute of Western Massachusetts | Western Massachusetts Community Case Data | [publichealthwm.org/covid-19/data/Franklin#Community](https://www.publichealthwm.org/covid-19/data/Franklin#Community) |
|  Massachusetts Department of Elementary and Secondary Education | Positive COVID-19 Cases in Schools | [doe.mass.edu/covid19/positive-cases/](https://www.doe.mass.edu/covid19/positive-cases/) |
|  New York Times | Coronavirus (Covid-19) Data in the United States | [github.com/nytimes/covid-19-data](https://github.com/nytimes/covid-19-data) |


#### Site Information

Site built in [GitLab Pages](https://docs.gitlab.com/ee/user/project/pages/), with [MkDocs](https://www.mkdocs.org/), based on the [Windmill theme](https://gristlabs.github.io/mkdocs-windmill/). [Favicon](https://thenounproject.com/icon/partner-511233/) from Tomas Knopp via the Noun Project.
