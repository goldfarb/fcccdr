## Versioning
***

The cumulative dataset of COVID-19 test and case data will be updated on weekdays, before 4 PM, with all data submitted through 10 AM that morning.

Any changes to previously submitted data will be indicated by a new `date`, and the previous data will be removed.

The Data Curator may make small changes, such as to formatting or for additional metadata cleanup, which will not be considered a change resulting in a changed `date`.

Changes to the [metadata elements](../metadata_elements) for any of the types of data will be noted, and the Data Curator will attempt to make previously submitted data work with the current scheme. 

> ***
For questions about versioning, please contact the Community Data Curator ([data_curator@...]()). Given time and workload limitations, questions from individuals may not be answered in a timely manner, as supporting the schools and districts is the focus of the   Franklin County Community COVID Data Repository.
