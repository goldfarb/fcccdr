## Access
***

This repository is created by and for the communities of Franklin County, anchored by people’s involvement with one of the county’s schools.

The materials collected are available to the entire community, with proper [attribution](../license/) for further use. All materials will be available as structured data, in a CSV format, combining information from the portal with other submissions. Along with single items, the cumulative dataset of test and case data is available as a CSV.

This repository is only as useful as the data collected, and so depends on community trust and involvement to continue. If you have any concerns about materials that are available through the repository, please contact the Data Curator.

The Data Curator will be working with school and district administrators to analyze and visualize the data collected in the repository. Further information is available [here](../engagement).

> ***
For questions about access, please contact the Community Data Curator ([data_curator@...]()). Given time and workload limitations, questions from individuals may not be answered in a timely manner, as supporting the schools and districts is the focus of the   Franklin County Community COVID Data Repository.
